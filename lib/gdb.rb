require_relative 'gdb-mi-parser/manual_parser_optimized.rb'

class GDB
	
	class Error < StandardError
		attr_reader :message
		def initialize(gdb_error_msg)
			@message = gdb_error_msg.value["msg"]
		end
	end
	
	public
	
	# Initialize GDB object.
	# Launches GDB with parameters from +gdb_options+ and sets up the interactions
	# +options+: :debug - output debug information to STDERR
	#            :gdb - replace "gdb" in gdb invocation with something else
	def initialize(gdb_options, options = {})
		options[:gdb] ||= "gdb"
		@gdb_io = IO.popen((["#{options[:gdb]} --interpreter=mi2 -n -q"] << gdb_options).join(" "), "w+")
		@debug = options[:debug]
		@messages = {}
		@token_queues = {}
		@handlers = {}
		@outbox = Queue.new
		@inbox = Queue.new
		
		@handler_thread = Thread.new do
			while (msg = @inbox.pop) != :quit
				@handlers.each do |k, h|
					h.call(msg)
				end
				@messages[msg.content_type] ||= Queue.new
				@messages[msg.content_type] << msg.value
				
			end
		end
		
		@gdb_thread = Thread.new do
			pr = ManualParser.new
			@gdb_io.each_line do |l|
				debug("<<< #{l}")
				# A horrifying hack to fix GDB violating its own syntax
				l.gsub!(/script={(.*?)}/, "script=[\\1]")
				pl = pr.parse(l)
 				debug("[#{pl.content_type}]	#{pl.inspect}")
				if pl.token
					@token_queues[pl.token] << (pl)
				end
				@inbox << pl
			end
		end
		
		@gdb_write_thread = Thread.new do
			while (msg = @outbox.pop) != :quit
				@gdb_io.puts(msg)
				debug(">>> #{msg}")
			end
		end
	end
	
	# Run a GDB command. Returns its result. Raises GDB::Error if failed.
	# +command+: Array or String, command to run
	def run(command)
		if command.is_a? Array
			command = command.join(" ")
		end
		if !(command.is_a? String)
			raise "String expected"
		end
		if command.include? "\n"
			raise "Single command expected"
		end
		token = rand(1e6)
		@token_queues[token] = Queue.new
		@outbox << (token.to_s + command)
		ret = (@token_queues[token]).pop
		if ret.content_type == "error"
			raise Error.new(ret)
		else
			return ret
		end
	end
	
	# Closes input, waits until IO handler finishes and returns all queued messages
	def finish
		@outbox << "quit"
 		@outbox << :quit
		@inbox << :quit
		@gdb_thread.join
		@gdb_write_thread.join
		@handler_thread.join
		messages = {}
		@messages.each do |k, v|
			messages[k] = []
			while v.size > 0
				messages[k] << v.pop
			end
		end
		@gdb_io.close
		@gdb_io = nil
		messages
	end
	
	# Run multiple commands, return the results. See GDB#run
	def run_multiple(commands)
		commands.map{|c| self.run c}
	end
	
	# Set a breakpoint at +location+
	# Options:
	#	+:commands+: GDB commands to run on breakpoint. See GDB documentation for format of such commands.
	#	+:condition+: breakpoint condition
	#	+:pending+: add a pending breakpoint if code is not yet available
	def breakpoint(location, options = {})
		r = self.run("-break-insert #{options[:pending] ? "-f" : ""} #{location}")
		br_id = r.value["bkpt"]["number"]
		if options[:commands]
			self.run("-break-commands #{br_id} #{options[:commands]}")
		end
		if options[:condition]
			self.run("-break-condition #{br_id} #{options[:condition]}")
		end
		
		return br_id
	end
	
	# Run a program with arguments +args+
	# Returns a hash with :exit_code = exit code of program or a signal name, :frames: array of breakpoint hit information
	def run_program(args)
		frames = []
		breakpoint_handler = Proc.new do |pl|
			if pl.content_type == "exec" && pl.value["event"] == "stopped" && pl.value["info"]["reason"] == "breakpoint-hit"
				frames << self.run("-stack-list-frames").value["stack"].to_a
				self.run("-exec-continue")
			end
		end
		stop_q = Queue.new
		stop_handler = Proc.new do |pl|
			if pl.content_type == "exec" && pl.value["event"] == "stopped"
				if pl.value["info"]["reason"] =~ /exited/
					stop_q << pl.value["info"]["exit-code"].to_s.to_i(8)
				elsif pl.value["info"]["reason"] =~ /signal-received/
					stop_q << pl.value["info"]["exit-code"] = pl.value["info"]["signal-name"]
				end
			end
		end
		h1 = add_handler breakpoint_handler
		h2 = add_handler stop_handler
		
		self.run("run #{args}")
		exit_data = stop_q.pop
		delete_handler h1
		delete_handler h2
		ret_data = {frames: frames}
		if exit_data.is_a? Integer
			ret_data[:exit_code] = exit_data
		else
			ret_data[:signal] = exit_data
		end
		return ret_data
	end
	
	private
	
	def debug(s)
		if @debug
			STDERR.puts s
		end
	end
	
	def add_handler(handler, handler_name = nil)
		if handler_name == nil
			while @handlers[handler_name]
				handler_name = rand(1e5)
			end
		end
		@handlers[handler_name] = handler
		return handler_name
	end
	
	def delete_handler(handler_name)
		@handlers.delete handler_name
	end
end
