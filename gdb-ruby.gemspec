Gem::Specification.new do |s|
  s.name        = 'gdb-ruby'
  s.version     = '2.2.0'
  s.date        = '2019-07-17'
  s.summary     = "gdb-ruby"
  s.description = "gdb ruby interface"
  s.authors     = ["Iaroslav Gridin"]
  s.email       = 'voker57@gmail.com'
  s.files       = Dir.glob("**/*.rb")
  s.homepage    =
    'https://gitlab.com/nisec/gdb-ruby'
  s.license       = 'MIT'
end
